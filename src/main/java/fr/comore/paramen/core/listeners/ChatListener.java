package fr.comore.paramen.core.listeners;

import fr.comore.paramen.rank.accounts.Account;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        Account account = Account.getAccount(player);

        event.setFormat(account.getRank().getPrefix()+"%1$s §8:§f %2$s");
    }

}
