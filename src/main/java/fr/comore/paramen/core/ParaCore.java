package fr.comore.paramen.core;

import fr.comore.paramen.core.cmds.AnnounceCommand;
import fr.comore.paramen.core.cmds.MSGCommand;
import fr.comore.paramen.core.cmds.REPLYCommand;
import fr.comore.paramen.core.listeners.ChatListener;
import fr.comore.paramen.core.listeners.FoodChangeLevel;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class ParaCore extends JavaPlugin {

    private static long channelId = 435530420751433739L;

    private static Map<Player, Player> replyers = new HashMap<>();

    private static ParaCore instance;

    @Override
    public void onEnable() {
        instance = this;
        registerComponents();
        super.onEnable();
    }

    /*@Override
    public void onLoad() {
        try {
            jda = new JDABuilder(AccountType.BOT).setToken("NDM3MzE3MDg5ODIxMTk2Mjkw.Db0Y4w.LlQRSkc_o7A03LPPd5Usjq7AoRk").addEventListener(new DiscordListener()).buildAsync();
        } catch (LoginException e) {
            e.printStackTrace();
        }
        super.onLoad();
    }*/

    /*@Override
    public void onDisable() {
        jda.shutdown();
        for(Player player : Bukkit.getServer().getOnlinePlayers()) {
            player.kickPlayer("§6Sorry, but the server is reloading. Please wait a minute.");
        }
        super.onDisable();
    }*/

    private void registerComponents() {
        PluginManager pluginManager = Bukkit.getServer().getPluginManager();
        pluginManager.registerEvents(new ChatListener(), this);
        pluginManager.registerEvents(new FoodChangeLevel(), this);
        getCommand("announce").setExecutor(new AnnounceCommand());
        getCommand("reply").setExecutor(new REPLYCommand());
        getCommand("r").setExecutor(new REPLYCommand());
        getCommand("msg").setExecutor(new MSGCommand());
        getCommand("m").setExecutor(new MSGCommand());
        getCommand("message").setExecutor(new MSGCommand());
    }

    public static long getChannelId() {
        return channelId;
    }

    public static ParaCore getInstance() {
        return instance;
    }

    public void sendMessageToDiscord() {

    }

    public void sendMessageToMinecraft() {

    }

    public static Map<Player, Player> getReplyers() {
        return replyers;
    }
}
