package fr.comore.paramen.core.cmds;

import fr.comore.paramen.core.ParaCore;
import fr.comore.paramen.practice.player.PracticePlayer;
import fr.comore.paramen.rank.accounts.Account;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MSGCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;
            PracticePlayer practicePlayer = PracticePlayer.getAccount(player);
            Account account = Account.getAccount(player);

            if(strings.length >= 2) {
                if(Bukkit.getPlayer(strings[0]) == null) {
                    player.sendMessage("§6Error: This player isn't online.");
                    return false;
                }

                Player target = Bukkit.getPlayer(strings[0]);

                StringBuilder stringBuilder = new StringBuilder();
                for(String string : strings) {
                    if(string != strings[0]) {
                        stringBuilder.append(string + " ");
                    }
                }

                player.sendMessage("§6(§eYou §6» " + Account.getAccount(target).getRank().getPrefix() + target.getName() + "§6) §f" + stringBuilder.toString());
                target.sendMessage("§6("+Account.getAccount(target).getRank().getPrefix() + target.getName()+" §6» §eYou§6) §f" + stringBuilder.toString());
                ParaCore.getReplyers().put(player, target);
                ParaCore.getReplyers().put(target, player);
            } else {
                player.sendMessage("§6Usage: /" + command.getName() + " <player> <message>: §eSend a PM to a player. (PM = Private message)");
            }
        }

        return false;
    }
}
