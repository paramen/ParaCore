package fr.comore.paramen.core.cmds;

import fr.comore.paramen.core.ParaCore;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class REPLYCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player) {
            Player player = (Player) commandSender;

            if(strings.length >= 1) {
                if(ParaCore.getReplyers().containsKey(player)) {
                    if(ParaCore.getReplyers().get(player) == null) {
                        player.sendMessage("§6Error: This player isn't online.");
                        return false;
                    }

                    StringBuilder stringBuilder = new StringBuilder();
                    for(String string : strings) {
                        stringBuilder.append(string + " ");
                    }
                    player.performCommand("msg " + ParaCore.getReplyers().get(player).getName() + " " + stringBuilder.toString());
                } else {
                    player.sendMessage("§6Error: You can't reply to the void.");
                }
            } else {
                player.sendMessage("§6Usage: /" + command.getName() + " <message>: §eReply to a PM of a player. (PM = Private message)");
            }
        }

        return false;
    }
}
