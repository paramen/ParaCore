package fr.comore.paramen.core.cmds;

import fr.comore.paramen.core.ParaCore;
import fr.comore.paramen.rank.accounts.Account;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AnnounceCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

        if(commandSender instanceof Player && Account.getAccount((Player) commandSender).getRank().getPower() < 70) {
            commandSender.sendMessage("§6Error: You don't have the necessary permissions.");
            return true;
        }

        if(strings.length >= 1) {
            StringBuilder stringBuilder = new StringBuilder();
            for(String string : strings) {
                stringBuilder.append(string + " ");
            }

            String name = commandSender instanceof Player ? Account.getAccount((Player) commandSender).getRank().getPrefix() + commandSender.getName() : "System";

            Bukkit.broadcastMessage("§8[§e§lP§r§eara§fmen§8] §6" + name + "§8: §6"+stringBuilder.toString().replace("&", "§"));

        } else {
            commandSender.sendMessage("§6Usage: /announce <message>");
        }

        return false;
    }
}
